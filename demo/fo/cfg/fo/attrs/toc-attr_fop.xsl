<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
                xmlns:fo="http://www.w3.org/1999/XSL/Format"
                version="2.0">

  <xsl:attribute-set name="__toc__topic__content">
    <xsl:attribute name="text-align">justify</xsl:attribute>
  </xsl:attribute-set>
  
</xsl:stylesheet>
